package websocket;

import java.io.IOException;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import bean.Mass;
import net.sf.json.JSONObject;


@ServerEndpoint("/websocket")
public class WebSocket {

	private static ConcurrentHashMap<String,WebSocket> map = new ConcurrentHashMap<>();
	private Session session;
	private String name;
	
	@OnOpen
	public void onOpen(Session session) {
		this.session = session;
		Mass mass1 = new Mass();
		mass1.setStatu("system");
		JSONObject obj1;
		String message1;
		for(String key:map.keySet()) {
			try {
				mass1.setName(key);
				obj1 = new JSONObject().fromObject(mass1);
				message1 = obj1.toString();
				this.sendMessage(message1);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	@OnClose
	public void onClose() {
		Collection<WebSocket> col = map.values();
		while(true == col.contains(this)) {
			col.remove(this);
			System.out.println(map);
		}
		Mass mass = new Mass();
		mass.setStatu("system");
		mass.setName(name);
		mass.setTo("all");
		mass.setMassage("close");
		JSONObject obj = new JSONObject().fromObject(mass);
		String message = obj.toString();
		broadCast(message);
		//webSocketSet.remove(this);
		//subOnlineCount();
	}
	
	//广播消息
	public void broadCast(String message) {
		for(String key:map.keySet()) {
			try {
				map.get(key).sendMessage(message);
				System.out.println("广播消息："+message);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	@OnMessage
	public void onMessage(String message,Session session) {
		//System.out.println("来自客户端的消息"+message);
		JSONObject obj = new JSONObject().fromObject(message);
		Mass mass = (Mass) new JSONObject().toBean(obj,Mass.class);
		System.out.println("来自客户端的消息"+mass);
		if(mass.getStatu().equals("system")) {
			if(map.get(mass.getName())==null) {
				Mass mass1 = new Mass();
				mass1.setStatu("system");
				mass1.setName(mass.getName());
				JSONObject obj1 = new JSONObject().fromObject(mass1);
				String message1 = obj1.toString();
				broadCast(message1);
				name = mass.getName();
				map.put(name, this);
				System.out.println(map);
			}
			
		}else {
			try {
				
				map.get(mass.getTo()).sendMessage(message);
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		}
//		for(MyWebSocket item:webSocketSet) {
//			try {
//				item.sendMessage(message);
//			}catch(Exception e) {
//				e.printStackTrace();
//				continue;
//			}
//		}
	}
	
	@OnError
	public void onError(Session session,Throwable error) {
		System.out.println("发生了错误");
		error.printStackTrace();
	}
	public void sendMessage(String message) throws IOException{
        //this.session.getBasicRemote().sendText(message);
        this.session.getAsyncRemote().sendText(message);
    }
}
