package bean;

public class Mass {
	private String name;
	private String to;
	private String statu;
	private String massage;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getStatu() {
		return statu;
	}
	public void setStatu(String statu) {
		this.statu = statu;
	}
	public String getMassage() {
		return massage;
	}
	public void setMassage(String massage) {
		this.massage = massage;
	}
	@Override
	public String toString() {
		return "Mass [name=" + name + ", to=" + to + ", statu=" + statu + ", massage=" + massage + "]";
	}
	
}
