# learnWebSocket

#### 介绍

基于websocket的简单在线聊天室（比较粗糙）

#### 软件架构

eclipse开发
java version 8.0
tomcat version 8.0
html+servlet+websocket+ajax 
1. 使用HTML+CSS构建简单页面布局
2. 使用servlet实现简单登录（没有使用数据库，登录名任意并需要自己保证不重复，密码为‘123’）
3. 使用ajax获取登录用户名（不知道websocket如何直接拿到servlet里的session，所有用ajax请求来拿然后传递给后端websocket程序）
4. websocket实现在线端对端聊天。


#### 安装教程


#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献



#### 特技

